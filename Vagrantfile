# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  config.vm.box = "ubuntu-16.04"
  config.vm.provider "virtualbox" do |vb|
    vb.cpus = 1
    vb.memory = 1024
  end

  # Provision masters followers
  (1..2).each do |i|
    config.vm.define "master#{i}" do |master|
      master.vm.hostname = "master#{i}"
      master.vm.network "private_network", ip: "10.10.18.1#{i}"
    end
  end

  # Provision workers
  (1..2).each do |i|
    config.vm.define "worker#{i}" do |worker|
      worker.vm.hostname = "worker#{i}"
      worker.vm.network "private_network", ip: "10.10.18.2#{i}"
    end
  end

  # Provision master leader
  config.vm.define "master" do |master|
    master.vm.hostname = "master"
    master.vm.network "private_network", ip: "10.10.18.10"
    master.vm.provision "shell", path: "scripts/ssh_keys_generate.sh", privileged: false
    master.vm.provision :ansible_local do |ansible|
      ansible.provisioning_path = "/vagrant/ansible" 
      ansible.inventory_path    = "hosts"
      ansible.playbook          = "swarm.yml"
      ansible.limit             = "all"
      ansible.verbose           = false
      ansible.install           = false
    end

  end
end
