echo "Generate ssh key for master"
ssh-keygen -b 4096 -t rsa -f ~/.ssh/id_rsa -q -N ""

echo "Copy ssh keys to the masters and workers nodes"
sshpass -p vagrant ssh-copy-id -i ~/.ssh/id_rsa.pub -o StrictHostKeyChecking=no 10.10.18.11
sshpass -p vagrant ssh-copy-id -i ~/.ssh/id_rsa.pub -o StrictHostKeyChecking=no 10.10.18.12
sshpass -p vagrant ssh-copy-id -i ~/.ssh/id_rsa.pub -o StrictHostKeyChecking=no 10.10.18.21
sshpass -p vagrant ssh-copy-id -i ~/.ssh/id_rsa.pub -o StrictHostKeyChecking=no 10.10.18.22